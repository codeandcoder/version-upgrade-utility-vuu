package com.wordpress.rasanti.vuu.view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;

import com.wordpress.rasanti.vuu.controller.Controller;
import com.wordpress.rasanti.vuu.model.LICENSEFile;
import com.wordpress.rasanti.vuu.model.READMEFile;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Santi Ruiz
 */
public class GUIFrame extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
                        
    private Controller controller;
    
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JScrollPane jScrollPane3;
    private JScrollPane jScrollPane4;
    private JButton jbAddAuthor;
    private JButton jbAddChange;
    private JButton jbSaveAndExit;
    private DefaultListModel<String> jlChangelogModel;
    private DefaultListModel<String> jlAuthorsModel;
    private JList<String> jlAuthors;
    private JList<String> jlChangelog;
    private JTextField jrfVersion;
    private JTextArea jtaLicense;
    private JTextArea jtaReadme;
    private JTextField jtfAuthor;
    private JTextField jtfChange;
    private JTextField jtfPattern;
    private JTextField jtfProjectName;     
    
    /**
     * Creates new form GUIFrame
     */
    public GUIFrame() {
        initComponents();
    }
    
    private void initComponents() {

        controller = new Controller();
        jPanel1 = new JPanel();
        jLabel3 = new JLabel();
        jtfPattern = new JTextField();
        jLabel4 = new JLabel();
        jScrollPane2 = new JScrollPane();
        jtaLicense = new JTextArea();
        jPanel2 = new JPanel();
        jtfProjectName = new JTextField();
        jLabel1 = new JLabel();
        jrfVersion = new JTextField();
        jPanel3 = new JPanel();
        jLabel2 = new JLabel();
        jScrollPane1 = new JScrollPane();
        jtaReadme = new JTextArea();
        jPanel4 = new JPanel();
        jLabel6 = new JLabel();
        jScrollPane4 = new JScrollPane();
        jtfAuthor = new JTextField();
        jbAddAuthor = new JButton();
        jPanel5 = new JPanel();
        jLabel5 = new JLabel();
        jScrollPane3 = new JScrollPane();
        jtfChange = new JTextField();
        jbAddChange = new JButton();
        jbSaveAndExit = new JButton();
        jlChangelogModel = new DefaultListModel<String>();
        jlAuthorsModel = new DefaultListModel<String>();
        for (int i = 0; i < controller.getChangelog().getLastChanges().size(); i++) {
            String ch = controller.getChangelog().getLastChanges().get(i);
            jlChangelogModel.addElement(ch);
        }
        for (int i = 0; i < controller.getAuthors().getAuthors().size(); i++) {
            String au = controller.getAuthors().getAuthors().get(i);
            jlAuthorsModel.addElement(au);
        }
        jlAuthors = new JList<String>(jlAuthorsModel);
        jlChangelog = new JList<String>(jlChangelogModel);
        
        jbAddChange.addActionListener(this);
        jbAddAuthor.addActionListener(this);
        jbSaveAndExit.addActionListener(this);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("VUU");

        jPanel1.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        jLabel3.setText("LICENSE");

        jtfPattern.setText(controller.getLicense().getProgramNameReplPattern());

        jLabel4.setText("pattern");

        jtaLicense.setColumns(20);
        jtaLicense.setRows(5);
        jScrollPane2.setViewportView(jtaLicense);
        jtaLicense.setText(controller.getLicense().getLicense());

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel3)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtfPattern, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(jtfPattern, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        jtfProjectName.setText(controller.getReadme().getProgramName());

        jLabel1.setText("v.");

        jrfVersion.setText(controller.getReadme().getCurrentVersion());

        GroupLayout jPanel2Layout = new GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jtfProjectName, GroupLayout.PREFERRED_SIZE, 440, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jrfVersion, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 189, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(jtfProjectName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel1)
                .addComponent(jrfVersion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        jLabel2.setText("README Commentary");

        jtaReadme.setColumns(20);
        jtaReadme.setRows(5);
        jScrollPane1.setViewportView(jtaReadme);
        jtaReadme.setText(controller.getReadme().getCommentary());

        GroupLayout jPanel3Layout = new GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        jPanel4.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        jLabel6.setText("AUTHORS");
        
        jScrollPane4.setViewportView(jlAuthors);

        jtfAuthor.setText("Author Name <email@email.com");

        jbAddAuthor.setText("Add");

        GroupLayout jPanel4Layout = new GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jtfAuthor)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbAddAuthor)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfAuthor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbAddAuthor))
                .addContainerGap())
        );

        jPanel5.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        jLabel5.setText("CHANGELOG");

        jScrollPane3.setViewportView(jlChangelog);

        jtfChange.setText("change");

        jbAddChange.setText("Add");

        GroupLayout jPanel5Layout = new GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jtfChange)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbAddChange)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfChange, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbAddChange))
                .addContainerGap())
        );

        jbSaveAndExit.setText("Save and Exit");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(jbSaveAndExit, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbSaveAndExit)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }                      

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUIFrame().setVisible(true);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource().equals(jbAddChange)) {
            if ( !"".equals(jtfChange.getText().trim()) ) {
                jlChangelogModel.add(0,jtfChange.getText().trim());
            }
        } else if (event.getSource().equals(jbAddAuthor)) {
            if ( !"".equals(jtfAuthor.getText().trim()) ) {
                jlAuthorsModel.add(0,jtfAuthor.getText().trim());
            }
        } else if (event.getSource().equals(jbSaveAndExit)) {
            if ( !"".equals(jtfProjectName.getText().trim()) &&
                    !"".equals(jrfVersion.getText().trim()) &&
                    !jrfVersion.getText().trim().equals(controller.getReadme().getCurrentVersion())) {
                
                String programName = jtfProjectName.getText().trim();
                String version = jrfVersion.getText().trim();
                String comment = jtaReadme.getText().trim();
                String pattern = jtfPattern.getText().trim();
                String lic = jtaLicense.getText().trim();
                List<String> changes = new ArrayList<String>();
                for (int i = 0; i < jlChangelogModel.size(); i++) {
                    changes.add(jlChangelogModel.get(i));
                }
                List<String> auths = new ArrayList<String>();
                for (int i = 0; i < jlAuthorsModel.size(); i++) {
                    auths.add(jlAuthorsModel.get(i));
                }
                
                READMEFile readme = controller.getReadme();
                readme.setProgramName(programName);
                readme.setCurrentVersion(version);
                readme.setCommentary(comment);
                LICENSEFile license = controller.getLicense();
                license.setProgramNameReplPattern(pattern);
                license.setLicense(lic);
                controller.getChangelog().setLastChanges(changes);
                controller.getAuthors().setAuthors(auths);
                controller.saveFiles(programName, version);
                System.exit(0);
            }
        }
    }
         
}
