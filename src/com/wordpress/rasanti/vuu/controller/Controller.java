package com.wordpress.rasanti.vuu.controller;

import com.wordpress.rasanti.vuu.model.AUTHORSFile;
import com.wordpress.rasanti.vuu.model.CHANGELOGFile;
import com.wordpress.rasanti.vuu.model.LICENSEFile;
import com.wordpress.rasanti.vuu.model.READMEFile;

public class Controller {
    
    public static final String FOLDER_PATH = ".";
    private CHANGELOGFile changelog;
    private READMEFile readme;
    private LICENSEFile license;
    private AUTHORSFile authors;
    
    public Controller() {
        Init();
    }
    
    private void Init() {
        readme = READMEFile.LoadFile(FOLDER_PATH);
        changelog = CHANGELOGFile.LoadFile(FOLDER_PATH);
        license = LICENSEFile.LoadFile(FOLDER_PATH);
        authors = AUTHORSFile.LoadFile(FOLDER_PATH);
    }
    
    public void saveFiles(String programName, String currentVersion) {
        readme.SaveFile(programName, currentVersion);
        changelog.SaveFile(programName, currentVersion);
        license.SaveFile(programName, currentVersion);
        authors.SaveFile(programName, currentVersion);
    }

    // GETTERS && SETTERS
    public CHANGELOGFile getChangelog() {
        return changelog;
    }

    public void setChangelog(CHANGELOGFile changelog) {
        this.changelog = changelog;
    }

    public READMEFile getReadme() {
        return readme;
    }

    public void setReadme(READMEFile readme) {
        this.readme = readme;
    }

    public LICENSEFile getLicense() {
        return license;
    }

    public void setLicense(LICENSEFile license) {
        this.license = license;
    }

    public AUTHORSFile getAuthors() {
        return authors;
    }

    public void setAuthors(AUTHORSFile authors) {
        this.authors = authors;
    }

}
