package com.wordpress.rasanti.vuu.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.wordpress.rasanti.vuu.view.GUIFrame;

public class LICENSEFile {

    public static final String FILE_NAME = "/LICENSE";
    private String folderPath;
    private String license;
    private String programNameReplPattern;
    
    private LICENSEFile() {
        
    }
    
    public static LICENSEFile LoadFile(String path) {
        LICENSEFile license = new LICENSEFile();
        license.setFolderPath(path);
        File file = new File(path + FILE_NAME);
        if (!file.exists()) {
            license.setProgramNameReplPattern("#@");
            license.setLicense("#@ has no license");
        } else {
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                // Skip first line.
                String line = br.readLine();
                line = br.readLine();
                StringBuilder lice = new StringBuilder();
                while (line != null) {
                    lice.append(line + "\n");
                    line = br.readLine();
                }
                license.setProgramNameReplPattern("#@");
                license.setLicense(lice.toString());
                br.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
            }
        }

        return license;
    }

    public void SaveFile(String programName, String currentVersion) {
        File file = new File(folderPath + FILE_NAME);
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(file));
            pw.println(programName + " v" + currentVersion);
            pw.print(license.replaceAll(programNameReplPattern, programName));
            pw.close();
        } catch (IOException ex) {
            Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
        }
    }

    // GETTERS && SETTERS
    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getProgramNameReplPattern() {
        return programNameReplPattern;
    }

    public void setProgramNameReplPattern(String programNameReplPattern) {
        this.programNameReplPattern = programNameReplPattern;
    }
    
}
