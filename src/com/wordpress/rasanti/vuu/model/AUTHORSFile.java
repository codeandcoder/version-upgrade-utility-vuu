package com.wordpress.rasanti.vuu.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.wordpress.rasanti.vuu.view.GUIFrame;

public class AUTHORSFile {

    public static final String FILE_NAME = "/AUTHORS";
    private String folderPath;
    private List<String> authors;
    
    private AUTHORSFile() {
        
    }
    
    public static AUTHORSFile LoadFile(String path) {
        AUTHORSFile authors = new AUTHORSFile();
        authors.setFolderPath(path);
        File file = new File(path + FILE_NAME);
        if (!file.exists()) {
            authors.setAuthors(new ArrayList<String>());
        } else {
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                // Skip first line
                String line = br.readLine();
                line = br.readLine();
                List<String> auth = new ArrayList<>();
                while (line != null) {
                    auth.add(line);
                    line = br.readLine();
                }
                authors.setAuthors(auth);
                br.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
            }
        }

        return authors;
    }

    public void SaveFile(String programName, String currentVersion) {
        File file = new File(folderPath + FILE_NAME);
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(file));
            pw.println(programName + " v" + currentVersion);
            for (int i = 0; i < authors.size(); i++) {
                pw.println(authors.get(i));
            }
            pw.close();
        } catch (IOException ex) {
            Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
        }
    }

    // GETTERS && SETTERS
    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }
}
