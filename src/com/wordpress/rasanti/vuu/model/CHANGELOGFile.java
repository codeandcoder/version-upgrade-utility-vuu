package com.wordpress.rasanti.vuu.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.wordpress.rasanti.vuu.view.GUIFrame;

public class CHANGELOGFile {

    public static final String FILE_NAME = "/CHANGELOG";
    private String folderPath;
    private List<String> lastChanges;

    private CHANGELOGFile() {

    }

    public static CHANGELOGFile LoadFile(String path) {
        CHANGELOGFile changelog = new CHANGELOGFile();
        changelog.setFolderPath(path);
        File file = new File(path + FILE_NAME);
        if (!file.exists()) {
            changelog.setLastChanges(new ArrayList<String>());
        } else {
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                // Skip first line
                String line = br.readLine();
                line = br.readLine();
                List<String> changes = new ArrayList<>();
                while (line != null) {
                    changes.add(line);
                    line = br.readLine();
                }
                changelog.setLastChanges(changes);
                br.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
            }
        }

        return changelog;
    }

    public void SaveFile(String programName, String currentVersion) {
        File file = new File(folderPath + FILE_NAME);
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(file));
            pw.println(programName + " v" + currentVersion);
            pw.println("*v" + currentVersion);
            for (int i = 0; i < lastChanges.size(); i++) {
                pw.println(lastChanges.get(i));
            }
            pw.close();
        } catch (IOException ex) {
            Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
        }
    }

    // GETTERS && SETTERS
    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public List<String> getLastChanges() {
        return lastChanges;
    }

    public void setLastChanges(List<String> lastChanges) {
        this.lastChanges = lastChanges;
    }

}
