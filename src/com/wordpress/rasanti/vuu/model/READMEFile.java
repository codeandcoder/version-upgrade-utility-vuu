package com.wordpress.rasanti.vuu.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.wordpress.rasanti.vuu.view.GUIFrame;

public class READMEFile {

    public static final String FILE_NAME = "/README";
    private String programName;
    private String currentVersion;
    private String folderPath;
    private String commentary;
    
    private READMEFile() {
        
    }
    
    public static READMEFile LoadFile(String path) {
        READMEFile readme = new READMEFile();
        readme.setFolderPath(path);
        File file = new File(path + FILE_NAME);
        if (!file.exists()) {
            readme.setProgramName("Program Name");
            readme.setCurrentVersion("0.1a");
            readme.setCommentary("This is a program.");
        } else {
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line = br.readLine();
                int delim = line.lastIndexOf('v');
                String programName = line.substring(0, delim).trim();
                String currentVersion = line.substring(delim+1).trim();
                StringBuilder comment = new StringBuilder();
                line = br.readLine();
                while (line != null) {
                    comment.append(line + "\n");
                    line = br.readLine();
                }
                readme.setProgramName(programName);
                readme.setCurrentVersion(currentVersion);
                readme.setCommentary(comment.toString());
                br.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
            }
        }

        return readme;
    }

    public void SaveFile(String programName, String currentVersion) {
        File file = new File(folderPath + FILE_NAME);
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(file));
            pw.println(programName + " v" + currentVersion);
            pw.print(commentary);
            pw.close();
        } catch (IOException ex) {
            Logger.getLogger(GUIFrame.class.getName()).log(Level.SEVERE,null, ex);
        }
    }

    // GETTERS && SETTERS
    public String getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }
    
    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }
    
    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }
    
}
